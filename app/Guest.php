<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    // Table Name
    protected $table = 'guests';
    // Primary Key
    public $primaryKey = 'guest_id';
    // Timestamps
    public $timestamps = true; 
    // Create a Relationship

    public function user() {

        return $this->belongsTo('App\User');

    }
}
