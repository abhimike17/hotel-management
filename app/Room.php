<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    // Table Name
    protected $table = 'rooms';
    // Primary Key
    public $primaryKey = 'assigned_room';
    // Timestamps
    public $timestamps = true; 
    // Create a Relationship

    public function guests() {

        return $this->belongsTo('App\Guest');

    }
}
