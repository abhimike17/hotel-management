<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\Guest;
use DB;

class BookingsController extends Controller
{

    public function __construct() {

        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::orderBy('created_at','desc')->get();
        //return $post = Post::where('title','Post Two')->get();
        //$posts = DB::select('SELECT * FROM posts');
        return view('bookings.index')->with('bookings', $bookings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bookings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'room_number' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'payment_info' => 'required',
            'check_in_time' => 'required',
            'check_out_time' => 'required',
            
        ]);
            // Insert into database
            $booking = new Booking;
            $booking->room_number = $request->input('room_number');
            $booking->first_name = $request->input('first_name');
            $booking->last_name = $request->input('last_name');
            $booking->payment_info = $request->input('payment_info');
            $booking->check_in_time = $request->input('check_in_time');
            $booking->check_out_time= $request->input('check_out_time');
            $booking->guest_id = DB::table('guests')->where('guests.assigned_room', 1)->pluck('guest_id')[0];
            $booking->save();

            // Redirect back to beginning with
            // a success message

            return redirect('/bookings')->with('success', 'Booking Logged');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $booking = Booking::find($id);
        return view('bookings.show')->with('booking',$booking);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = booking::find($id);
        return view('bookings.edit')->with('booking',$booking);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'room_number' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'payment_info' => 'required',
            'check_in_time' => 'required',
            'check_out_time' => 'required',
                    ]);
                        // Insert into database
                        $booking = Booking::find($id);
                        $booking->room_number = $request->input('room_number');
                        $booking->first_name = $request->input('first_name');
                        $booking->last_name = $request->input('last_name');
                        $booking->payment_info = $request->input('payment_info');
                        $booking->check_in_time = $request->input('check_in_time');
                        $booking->check_out_time= $request->input('check_out_time');
                        $booking->guest_id = DB::table('guests')->where('guests.assigned_room', 1)->pluck('guest_id')[0];
                        $booking->save();
            
                        // Redirect back to beginning with
                        // a success message
            
                        return redirect('/bookings')->with('success', 'Booking Logged');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $booking = Booking::find($id);
        $booking->delete();
        return redirect('/bookings')->with('success', 'Booking Log Removed');
        
    }
}
