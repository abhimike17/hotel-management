<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Guest;
use DB;

class RoomsController extends Controller
{

    public function __construct() {
        
                $this->middleware('auth');
        
            }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::orderBy('assigned_room','desc')->get();
        //return $post = Post::where('title','Post Two')->get();
        //$posts = DB::select('SELECT * FROM posts');
        return view('rooms.index')->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

                        'room_type' => 'required',
                        'availability' => 'required',
                        'clean' => 'required',
                        'damaged' => 'required'
                        
                    ]);
                        // Insert into database
                        $room = new Room;
                        $room->room_number = $request->input('room_number');
                        $room->room_type = $request->input('room_type');
                        $room->availability = $request->input('availability');
                        $room->clean = $request->input('clean');
                        $room->damaged = $request->input('damaged');
                        $room->guest_id = DB::table('guests')->where('guests.assigned_room', $request->input('room_number'))->pluck('guest_id')[0];
                        //$room->guest_id = DB::select('SELECT guests.guest_id FROM guests, rooms WHERE guests.assigned_room = rooms.assigned_room');
                        $room->save();
            
                        // Redirect back to beginning with
                        // a success message
            
                        return redirect('/rooms')->with('success', 'Room Logged');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($assigned_room)
    {
        $room = Room::find($assigned_room);
        return view('rooms.show')->with('room',$room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($assigned_room)
    {
        $room = Room::find($room_id);
        return view('rooms.edit')->with('room',$room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $assigned_room)
    {
        $this->validate($request, [
            
                                    'room_type' => 'required',
                                    'availability' => 'required',
                                    'clean' => 'required',
                                    'damaged' => 'required'
                                    
                                ]);
                                    // Insert into database
                                    $room = Room::find($assigned_room);
                                    $room->room_type = $request->input('room_type');
                                    $room->availability = $request->input('availability');
                                    $room->clean = $request->input('clean');
                                    $room->damaged = $request->input('damaged');
                                    $room->guest_id = '0';
                                    $room->save();
                        
                                    // Redirect back to beginning with
                                    // a success message
                        
                                    return redirect('/rooms')->with('success', 'Room Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($assigned_room)
    {
        $room = Room::find($assigned_room);
        $room->delete();
        return redirect('/rooms')->with('success', 'Room Removed');
    }
}
