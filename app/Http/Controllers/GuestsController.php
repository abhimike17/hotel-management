<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Guest;
use DB;

class GuestsController extends Controller
{

    public function __construct() {

        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guests = Guest::orderBy('created_at','desc')->get();
        //return $post = Post::where('title','Post Two')->get();
        //$posts = DB::select('SELECT * FROM posts');
        return view('guests.index')->with('guests', $guests);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('guests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'first_name' => 'required',
            'last_name' => 'required',
            'DOB' => 'required',
            'group_size' => 'required',
            'duration_of_stay' => 'required',
            'assigned_room' => 'required',
        ]);
            // Insert into database
            $guest = new Guest;
            $guest->first_name = $request->input('first_name');
            $guest->last_name = $request->input('last_name');
            $guest->DOB = $request->input('DOB');
            $guest->group_size = $request->input('group_size');
            $guest->duration_of_stay = $request->input('duration_of_stay');
            $guest->assigned_room = $request->input('assigned_room');
            $guest->user_id = auth()->user()->id;
            $guest->save();

            // Redirect back to beginning with
            // a success message

            return redirect('/guests')->with('success', 'Guest Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($guest_id)
    {
        $guest = Guest::find($guest_id);
        return view('guests.show')->with('guest',$guest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($guest_id)
    {
        $guest = Guest::find($guest_id);
        return view('guests.edit')->with('guest',$guest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $guest_id)
    {
        $this->validate($request, [
            
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'DOB' => 'required',
                        'group_size' => 'required',
                        'duration_of_stay' => 'required',
                        'assigned_room' => 'required',
                    ]);
                        // Insert into database
                        $guest = Guest::find($guest_id);
                        $guest->first_name = $request->input('first_name');
                        $guest->last_name = $request->input('last_name');
                        $guest->DOB = $request->input('DOB');
                        $guest->group_size = $request->input('group_size');
                        $guest->duration_of_stay = $request->input('duration_of_stay');
                        $guest->assigned_room = $request->input('assigned_room');
                        $guest->save();
            
                        // Redirect back to beginning with
                        // a success message
            
                        return redirect('/guests')->with('success', 'Guest Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($guest_id)
    {
        $guest = Guest::find($guest_id);
        $guest->delete();
        return redirect('/guests')->with('success', 'Guest Removed');
        
    }
}
