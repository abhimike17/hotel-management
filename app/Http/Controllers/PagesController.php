<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PagesController extends Controller
{
    public function index() {
        $title = 'Hotel Management';
        return view('pages.index')->with('title', $title);

    }
    
    public function about() {
        $title = 'Manage Guests';
        return view('pages.about')->with('title', $title);
        
    }

    public function services() {
        $title = 'Manage Rooms';
        return view('pages.services')->with($title);
                
    }
    public function accounts() {
        $title = 'Manage Accounts';
        return view('pages.accounts')->with($title);
                
    }

    public function info() {
        $title = 'Manage Bookings';
        return view('pages.info')->with($title);
                
    }
}
