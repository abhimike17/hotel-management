@extends('layouts.app')

@section('content')
    <br>
    <a href="/info" class ="btn btn-default">Go Back</a>
    <h1>Reservations</h1>
    @if(count($bookings) > 0)
        @foreach($bookings as $booking)
            <div class="well">
                <h3><a href="/bookings/{{$booking->id}}"> {{$booking->first_name}}</a></h3>
                <small>Entered on {{$booking->created_at}}</small>
            </div>
        @endforeach
    @else
        <p>No Reservations Found</p>
    @endif
@endsection