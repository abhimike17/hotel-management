<!DOCTYPE html>

<html lang="en">
    
    <head>
                <h1>Bookings</h1>

<br>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- Load css styles -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/bootstrap-responsive.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/pluton.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/jquery.cslider.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/jquery.bxslider.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/animate.css') }}" rel="stylesheet" type="text/css" >
        <link href="{{ asset('css/pluton-ie7.css') }}" rel="stylesheet" type="text/css" >

   
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/ico/apple-touch-icon-144.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/ico/apple-touch-icon-114.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72.png">
        <link rel="apple-touch-icon-precomposed" href="/images/ico/apple-touch-icon-57.png">
        <link rel="shortcut icon" href="/images/ico/favicon.ico">
    </head>
    <body>
        @include('inc.navbar')
        <div class="container">
            @include('inc.messages')
            @yield('content')
        </div>

        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.mixitup.js"></script>
        <script type="text/javascript" src="js/bootstrap.js"></script>
        <script type="text/javascript" src="js/modernizr.custom.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.js"></script>
        <script type="text/javascript" src="js/jquery.cslider.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.inview.js"></script>
        <!-- Load google maps api and call initializeMap function defined in app.js -->
        <script async="" defer="" type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&callback=initializeMap"></script>
        <!-- css3-mediaqueries.js for IE8 or older -->
        <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="js/app.js"></script>
    </body>
</html>    