@extends('layouts.app')

@section('content')
    <br>
    <a href="/bookings" class ="btn btn-default">Go Back</a>
    <h2>Entered on {{$booking->created_at}}</h2>
    <h4>First Name : {{$booking->first_name}}</h4>
        <h4>Last Name : {{$booking->last_name}}</h4>
        <h4>Assigned Room : {{$booking->assigned_room}}</h4>
        <h4>Guest ID : {{$booking->guest_id}}</h4>
            <h4>Payment Information : {{$booking->payment_info}}</h4>
                <h4>Check In Date/Time : {{$booking->check_in_time}}</h4>
                    <h4>Check Out Date/Time : {{$booking->check_out_time}}</h4>
                        
    <hr>
    <!-- Add An Edit Link -->
    <a href="/bookings/{{$booking->id}}/edit" class = "btn btn-default">Edit</a>
    <!-- Add A Delete Link -->
    {!!Form::open(['action' => ['BookingsController@destroy', $booking->id], 'method' => 'POST', 'class' => 'pull-right' ])!!}
        {{Form::hidden('_method', 'DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn=danger'])}}
    {!!Form::close()!!}
@endsection