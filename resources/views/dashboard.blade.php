<!-- @extends('layouts.app')
 -->
@section('content')
<br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                   You Are Logged In

                </div>
                <a class="nav-link" href="/about">Guest Management Page<span class="sr-only">(current)</span></a>
                <br>
                <a class="nav-link" href="/info">Booking Management Page<span class="sr-only">(current)</span></a>
                <br>
                <a class="nav-link" href="/services">Room Management Page<span class="sr-only">(current)</span></a>
                <br>
                <a class="nav-link" href="/accounts">Account Management Page<span class="sr-only">(current)</span></a>
                <br>
            </div>
            @if(count($guests) > 0)
            <div class="card-header">Guests You Have Entered</div>
            <table class = "table table-striped">
                <tr>
                    <th>First Name</th><th>Last Name</th><th>ID</th>
                </tr>
                @foreach($guests as $guest)
                    <tr>
                        <th>{{$guest->first_name}}</th>
                        <th>{{$guest->last_name}}</th>
                        <th>{{$guest->guest_id}}</th>
                    </tr>
                @endforeach
            </table>
        </div>
        @else
        <div class="card-body">No Guests Entered</div>
        @endif
    </div>
</div>
@endsection
