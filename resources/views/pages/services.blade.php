@extends('layouts.app')

@section('content')
<!-- Portfolio section start -->
<div id="price" class="section secondary-section centered">
            <div class="container">
                <div class="title" >
                    <h1>Manage Rooms</h1>
                </div>
                <div class="price-table row-fluid centered">
                    <div class="span4 price-column">
                        <h3>Log Rooms</h3>
                        <a href="/rooms/create" class="button button-ps">Select</a>
                    </div>
                    <div class="span4 price-column">
                        <h3>View Rooms</h3>
                        <a href="/rooms/" class="button button-ps">Select</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!-- Price section end -->

@endsection
