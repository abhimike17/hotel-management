@extends('layouts.app')

@section('content')
<div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>User Accounts</h1>
                    <!-- Section's title goes here -->
                </div>
        <div class="section secondary-section">
            <div class="triangle"></div>
            <div class="container centered">
                <li><a href="/dashboard" class="button">View Your Dashboard</a></li>
            </div>
            <div class="container centered">
                <li><a href="/users/" class="button">View Users</a></li>
            </div>
        </div>
@endsection
