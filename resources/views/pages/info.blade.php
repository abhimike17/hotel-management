@extends('layouts.app')

@section('content')
<div class="section primary-section" id="service">
            <div class="container">
                <!-- Start title section -->
                <div class="title">
                    <h1>Reservations</h1>
                    <!-- Section's title goes here -->
                    <p>Live life with a little Spice</p>
                </div>
        <div class="section secondary-section">
            <div class="triangle"></div>
            <div class="container centered">
                <p class="large-text">Elegance is not the abundance of simplicity. It is the absence of complexity.</p>
                <li><a href="/bookings/create" class="button">Add Reservations</a></li>
            </div>
            <div class="container centered">
                <li><a href="/bookings/" class="button">View Reservations</a></li>
            </div>
        </div>



@endsection
