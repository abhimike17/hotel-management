@extends('users.custom')

@section('content')
    <br>
    <a href="/accounts" class ="btn btn-default">Go Back</a>
    <h1>Users</h1>
    @if(count($users) > 0)
        @foreach($users as $user)
            <div class="well">
                <h3><a href="/users/{{$user->id}}"> {{$user->name}}</a></h3>
                <small>Entered on {{$user->created_at}}</small>
            </div>
        @endforeach
    @else
        <p>No Users Found</p>
    @endif
@endsection