@extends('layouts.app')

@section('content')
    <br>
    <a href="/bookings" class ="btn btn-default">Go Back</a>
    <h1>Edit Reservation</h1>
    {!! Form::open(['action' => ['BookingsController@update', $booking->id], 'method' => 'POST']) !!}
        <div class = "form-group">

            {{Form::text('room_number', '', ['class' => 'form-control', 'placeholder' => 'Room Number'])}}
        </div>
        
        <div class = "form-group">

            {{Form::text('first_name', $booking->first_name, ['class' => 'form-control', 'placeholder' => 'First Name'])}}
        </div>
        <div class = "form-group">

            {{Form::text('last_name', $booking->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name'])}}
        </div>
        <div class = "form-group">

            {{Form::text('payment_info', $booking->payment_info, ['class' => 'form-control', 'placeholder' => 'Payment Information'])}}
        </div>
        <div class = "form-group">

            {{Form::text('check_in_time', $booking->check_in_time, ['class' => 'form-control', 'placeholder' => 'Check In Date/Time'])}}
        </div>
        <div class = "form-group">

            {{Form::text('check_out_time', $booking->check_out_time, ['class' => 'form-control', 'placeholder' => 'Check Out Date/Time'])}}
        </div>
        
            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Submit', ['class' =>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection