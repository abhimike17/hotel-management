@extends('rooms.custom')

@section('content')
    <br>
    <a href="/services" class ="btn btn-default">Go Back</a>
    <h1>Rooms: </h1>
    <br>
    @if(count($rooms) > 0)
        @foreach($rooms as $room)
            <div class="well">
                <h3>Room # {{$room->assigned_room}}</h3>
                <h4>Updated on: {{$room->created_at}}</h4>
                <h4>Room Number: {{$room->assigned_room}}</h4>
                <h4>Guest ID: {{$room->guest_id}}</h4>
                    <h4>Room Type : {{$room->room_type}}</h4>
                        <h4>Availability : {{$room->availability}}</h4>
                            <h4>Clean : {{$room->clean}}</h4>
                                <h4>Damaged : {{$room->damaged}}</h4>
            </div>
            <!-- Add An Edit Link -->

<!-- Add A Delete Link -->
{!!Form::open(['action' => ['RoomsController@destroy', $room->assigned_room], 'method' => 'POST', 'class' => 'pull-right' ])!!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn=danger'])}}
{!!Form::close()!!}
            <br>
        @endforeach
    @else
        <p>No Rooms Found</p>
    @endif
@endsection