@extends('rooms.custom')

@section('content')
    <br>
    <a href="/services" class ="btn btn-default">Go Back</a>
    <h1>Enter Room</h1>
    {!! Form::open(['action' => 'RoomsController@store', 'method' => 'POST']) !!}
        <div class = "form-group">

            {{Form::text('room_number', '', ['class' => 'form-control', 'placeholder' => 'room_number'])}}
        </div>
        <div class = "form-group">

            {{Form::text('room_type', '', ['class' => 'form-control', 'placeholder' => 'room_type'])}}
        </div>
        <div class = "form-group">

            {{Form::text('availability', '', ['class' => 'form-control', 'placeholder' => 'availability'])}}
        </div>
        <div class = "form-group">

            {{Form::text('clean', '', ['class' => 'form-control', 'placeholder' => 'clean'])}}
        </div>
        <div class = "form-group">

            {{Form::text('damaged', '', ['class' => 'form-control', 'placeholder' => 'damaged'])}}
        </div>
        
            {{Form::submit('Submit', ['class' =>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection



