@extends('guests.custom')

@section('content')
    <br>
    <a href="/about" class ="btn btn-default">Go Back</a>
    <h1>Enter Guest</h1>
    {!! Form::open(['action' => 'GuestsController@store', 'method' => 'POST']) !!}
        <div class = "form-control">

            {{Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => 'First Name'])}}
        </div>
        <div class = "form-control">

            {{Form::text('last_name', '', ['class' => 'form-control', 'placeholder' => 'Last Name'])}}
        </div>
        <div class = "form-control">

            {{Form::text('DOB', '', ['class' => 'form-control', 'placeholder' => 'Date of Birth'])}}
        </div>
        <div class = "form-control">

            {{Form::text('group_size', '', ['class' => 'form-control', 'placeholder' => 'Group Size'])}}
        </div>
        <div class = "form-control">

            {{Form::text('duration_of_stay', '', ['class' => 'form-control', 'placeholder' => 'Duration of Stay'])}}
        </div>
        <div class = "form-control">

            {{Form::text('assigned_room', '', ['class' => 'form-control', 'placeholder' => 'Assigned Room'])}}
        </div>
            {{Form::submit('Submit', ['class' =>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection


