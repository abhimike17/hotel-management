@extends('layouts.app')

@section('content')
    <br>
    <a href="/guests" class ="btn btn-default">Go Back</a>
    <h1>Edit Guest</h1>
    {!! Form::open(['action' => ['GuestsController@update', $guest->guest_id], 'method' => 'POST']) !!}
        <div class = "form-control">

            {{Form::text('first_name', $guest->first_name, ['class' => 'form-control', 'placeholder' => 'First Name'])}}
        </div>
        <div class = "form-control">

            {{Form::text('last_name', $guest->last_name, ['class' => 'form-control', 'placeholder' => 'Last Name'])}}
        </div>
        <div class = "form-control">

            {{Form::text('DOB', $guest->DOB, ['class' => 'form-control', 'placeholder' => 'Date of Birth'])}}
        </div>
        <div class = "form-control">

            {{Form::text('group_size', $guest->group_size, ['class' => 'form-control', 'placeholder' => 'Group Size'])}}
        </div>
        <div class = "form-control">

            {{Form::text('duration_of_stay', $guest->duration_of_stay, ['class' => 'form-control', 'placeholder' => 'Duration of Stay'])}}
        </div>
        <div class = "form-control">

            {{Form::text('assigned_room', $guest->assigned_room, ['class' => 'form-control', 'placeholder' => 'Assigned Room'])}}
        </div>
            {{Form::hidden('_method', 'PUT')}}
            {{Form::submit('Submit', ['class' =>'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection


