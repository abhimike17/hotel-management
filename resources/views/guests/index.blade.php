@extends('guests.custom')

@section('content')
    <br>

    <a href="/about" class ="btn btn-default">Go Back</a>
    <div class="container">
        <div class="span9 center contact-info">
            <h1>Guests</h1>
            @if(count($guests) > 0)
                @foreach($guests as $guest)
                    <div class="well">
                        <h3><a href="/guests/{{$guest->guest_id}}"> {{$guest->first_name}}</a></h3>
                        <small>Entered on {{$guest->created_at}}</small>
                    </div>
                @endforeach
            @else
                <p>No Guests Found</p>
            @endif
        </div>
    </div>
@endsection