@extends('guests.custom')

@section('content')
    <br>
    <a href="/guests" class ="btn btn-default">Go Back</a>
    <div class="container">
        <div class="span9 center contact-info">
            <a>Entered on {{$guest->created_at}}</a>
            <br>
            <a>Guest ID # : {{$guest->guest_id}}</a>
            <br>
            <a>First Name : {{$guest->first_name}}</a>
                <br>
                <a>Last Name : {{$guest->last_name}}</a>
                    <br>
                    <a>Date of Birth : {{$guest->DOB}}</a>
                        <br>
                        <a>Group Size : {{$guest->group_size}}</a>
                            
                            <br>
                            <a>Duration of Stay : {{$guest->duration_of_stay}}</a>
                                <br>
                                <a>Room Number: {{$guest->assigned_room}}</a>
            <hr>
            <!-- Add An Edit Link -->
            <a href="/guests/{{$guest->guest_id}}/edit" class = "btn btn-default">Edit</a>
            <!-- Add A Delete Link -->
            {!!Form::open(['action' => ['GuestsController@destroy', $guest->guest_id], 'method' => 'POST', 'class' => 'pull-right' ])!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn=danger'])}}
            {!!Form::close()!!}
@endsection